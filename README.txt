
=========================
    QUICK START GUIDE
=========================

To edit the content of your website open: /index.html

== Structure ==

Main page is divided into 5 blocks: Research, Supervisors, Publications,
Seminars and Personal. You can rename, remove or add blocks to your liking
by editing the two sections: "top_menu" (links) and "main_content" (body).

== Your details ==

Replace all occurances of [Name/Author/Institution/Keywords] etc. with
appropriate details. Don't forget to change the following line:
	emailE=('xxx@' + 'ncl.ac.uk')
by replacing xxx with your e-mail (do it for both "me" and "footer" sections).

== Body text ==

Replace the filler text of "Lorem ipsum..." my a meaningful content.

=========================
         OTHERS
=========================

== Profile picture ==

To change the profile picture replace a file: /index_files/profilePic.jpg
The image will be resized to 200x200 pixels, so square pictures work best.

== Style ==

If you feel more adventurous you can play around with /index_files/style.css
to alter the look and formating of your website.
